package com.directadmin;
import java.util.Properties;
class Fixer { 
	public static void main(String[] args) throws Exception{ 
		String daPath = "/usr/local/directadmin";
		String ipaddr = "127.0.0.1"
		if(args.length > 0){ 
			daPath = args[0]
			if(args.length == 2){ 
				ipaddr = args[1]
			}
		}
		
		if(!(new File(daPath).exists())){ 
			throw new Exception("Directory " + daPath + " does not exists")
		}
		List users = []
		File usersDir = new File(daPath + "/data/users")
		if(!usersDir.exists()) throw new Exception("Directory " + usersDir + " does not exists")
		usersDir.eachDir { 
			users.add(it.getName())
		}
		File domainList
		File domainConfig
		File httpConfig
		List domains = []
		Properties props;
		users.each { 
			HttpConfig htconf = new HttpConfig();
			domainList = new File(usersDir.absolutePath + "/" + it + "/domains.list")
			httpConfig = new File(usersDir.absolutePath + "/" + it + "/httpd.conf")
			domainList.eachLine { domainName ->
				domainConfig = new File(usersDir.absolutePath + "/" + it + "/domains/"+ domainName + ".conf")
				
				props = new Properties()
				props.load(domainConfig.newInputStream())
				VirtualHostTemplate vht = new VirtualHostTemplate(domainName, ipaddr, it, 80, false)
				htconf.addVirtualHost(vht);
				
			}
			httpConfig.write(htconf.toString())
		}
		//		println domains
		//println new VirtualHostTemplate("domain.com", "94.242.221.1", "admin", 80, false);
	}
}
