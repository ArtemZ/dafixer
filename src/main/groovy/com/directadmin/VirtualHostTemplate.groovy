package com.directadmin;
import groovy.text.SimpleTemplateEngine;

class VirtualHostTemplate { 
	String domainName;
	String ipaddr;
	String user;
	Integer port = 80;
	Boolean ssl = false;
	public VirtualHostTemplate(String domain, String ip, String username, Integer portNum, Boolean enableSsl){ 
		domainName = domain;
		ipaddr = ip;
		user = username;
		port = portNum;
		ssl = enableSsl;
	}
	def makeBinding(){
		Map binding = [ domainName : domainName, ipaddr : ipaddr, user : user, port : port]
		def engine = new SimpleTemplateEngine()
		def template
		if(ssl){ 
			template = engine.createTemplate(sslTemplate()).make(binding)
		} else { 
			template = engine.createTemplate(normalTemplate()).make(binding)
		}
		return template
	}
	String toString(){ 
		return makeBinding().toString()
	}
	String normalTemplate() { 
		return """
<VirtualHost ${ipaddr}:${port} >


	ServerName www.${domainName}
	ServerAlias www.${domainName} ${domainName}
	ServerAdmin webmaster@${domainName}
	DocumentRoot /home/${user}/domains/${domainName}/public_html/
	ScriptAlias /cgi-bin/ /home/${user}/domains/${domainName}/public_html/cgi-bin/
	
	UseCanonicalName OFF

	SuexecUserGroup ${user} ${user}
	CustomLog /var/log/httpd/domains/${domainName}.bytes bytes
	CustomLog /var/log/httpd/domains/${domainName}.log combined
	ErrorLog /var/log/httpd/domains/${domainName}.error.log

	

	<Directory /home/${user}/domains/${domainName}/public_html/>
		Options +Includes -Indexes

		php_admin_flag engine ON
		<IfModule !mod_php6.c>
			php_admin_flag safe_mode OFF
		</IfModule>
		php_admin_value sendmail_path '/usr/sbin/sendmail -t -i -f ${user}@${domainName}'


		php_admin_value open_basedir /home/${user}/:/tmp:/var/tmp:/usr/local/lib/php/


	</Directory>


	
</VirtualHost>
"""

	}
	String sslTemplate() { 
		return """
<VirtualHost ${ipaddr}:${port} >

	SSLEngine on
	SSLCertificateFile /etc/httpd/conf/ssl.crt/server.crt
	SSLCertificateKeyFile /etc/httpd/conf/ssl.key/server.key
		

	ServerName www.${domainName}
	ServerAlias www.${domainName} ${domainName}
	ServerAdmin webmaster@${domainName}
	DocumentRoot /home/${user}/domains/${domainName}/public_html/
	ScriptAlias /cgi-bin/ /home/${user}/domains/${domainName}/public_html/cgi-bin/

	UseCanonicalName OFF

	SuexecUserGroup ${user} ${user}
	CustomLog /var/log/httpd/domains/${domainName}.bytes bytes
	CustomLog /var/log/httpd/domains/${domainName}.log combined
	ErrorLog /var/log/httpd/domains/${domainName}.error.log

	

	<Directory /home/${user}/domains/${domainName}/public_html>
		Options +Includes -Indexes

		php_admin_flag engine ON
		<IfModule !mod_php6.c>
			php_admin_flag safe_mode OFF
		</IfModule>
		php_admin_value sendmail_path '/usr/sbin/sendmail -t -i -f ${user}@${domainName}'


		php_admin_value open_basedir /home/${user}/:/tmp:/var/tmp:/usr/local/lib/php/


	</Directory>


</VirtualHost>
"""
	}
}
